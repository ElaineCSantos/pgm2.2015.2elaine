package aula;


public class TesteTime {

    public static void main(String[] args) {
        Time t1 = new Time();
        System.out.println(t1.getHoras() == 0);
        System.out.println(t1.getMinutos() == 0);
        System.out.println(t1.getSegundos() == 0);
        
        Time t2 = new Time(17, 25, 32);
        System.out.println(t2.getHoras() == 17);
        System.out.println(t2.getMinutos() == 25);
        System.out.println(t2.getSegundos() == 32);
        
        Time t3 = new Time("17:25:32");
        System.out.println(t3.getHoras() == 17);
        System.out.println(t3.getMinutos() == 25);
        System.out.println(t3.getSegundos() == 32);
        
        t1.setTime(17, 25, 32);
        System.out.println(t1.getHoras() == 17);
        System.out.println(t1.getMinutos() == 25);
        System.out.println(t1.getSegundos() == 32);        
        System.out.println(t1.toString().equals("17:25:32"));
        
        Time t4 = new Time(2, 3, 5);
        System.out.println(t4.toString().equals("02:03:05"));
        
        t4.setTime(25, 61, 304);
        System.out.println(t4.toString().equals("02:06:04"));
        
        Time t5 = new Time(10,42,36);
        System.out.println(t5.toString());
        t5.tick();
        System.out.println(t5.toString());
        
        Time t6 = new Time(800);
        System.out.println(t6.toString());
        t6.tickSeg();
        System.out.println(t6.toString());
        
        
    }


}
