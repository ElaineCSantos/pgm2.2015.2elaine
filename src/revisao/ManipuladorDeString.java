package revisao;

import java.util.Arrays;


public class ManipuladorDeString {
    
    /**
     * Exercício 4.
     * Escreva um método chamado substring, que receba como parâmetros um array
     * de char c e dois números a e b, e retorne um novo array contendo somente
     * os elementos de c que estiverem entre as posições a e b-1.
     */
    public static char[] substring(char[] str, int de, int até) {        
        if (de < 0 || até < 0 || de >= str.length || até >= str.length || até - de < 0) {
            return null;
        }
        
        char[] result = new char[até-de];

        for (int i = 0; de < str.length && de < até; de++, i++) {
            result[i] = str[de];
        }
        return result;
    }


    public static boolean contemAPartirDe(char[] str, char[] procurado, int posicaoInicialStr) {
        int i = 0;
        for (; posicaoInicialStr < str.length && i < procurado.length; i++) {
            if (str[posicaoInicialStr++] != procurado[i]) {
                return false;
            }
        }
        // str[] pode terminar antes de procurado[]
        return i == procurado.length;
    }
    
    
    /**
     * Exercício 5.
     * Escreva um método chamado contem, que receba como parâmetro dois arrays
     * de char (a e b) e retorne um valor booleano indicando se b está contido
     * em a.
     */
    public static boolean contem(char[] str, char[] procurado) {
        for (int i = 0; i < str.length; i++) {
            if (contemAPartirDe(str, procurado, i)) {
                return true;
            }
        }
        return false;
    }

    
    /**
     * Exercício 6.
     * Escreva um método chamado indexOf, que receba como parâmetro dois arrays
     * de char (a e b) e, se b estiver contido em a, retorne a posição de a na
     * qual a sequência de valores de b se inicia.
     */
    private static int indexOf(char[] str, char[] procurado) {
        for (int i = 0; i < str.length; i++) {
            if (contemAPartirDe(str, procurado, i)) {
                return i;
            }
        }
        return -1;
    }


    public static void main(String[] args) {
        char[] origem = {'o', 'l', 'a', ' ', 'm', 'u', 'n', 'd', 'o'};
        
        // Testes do método contem()
        System.out.println("Contém funcionando? " + (contem(origem, new char[] {'m', 'u', 'n'}) == true));
        System.out.println("Contém funcionando? " + (contem(origem, new char[] {'o', 'l', 'a', ' ', 'm', 'u', 'n', 'd', 'o'}) == true));
        System.out.println("Contém funcionando? " + (contem(origem, new char[] {'a', 'b', 'c'}) == false));
        System.out.println("Contém funcionando? " + (contem(origem, new char[] {'m', 'o', 'n'}) == false));
        System.out.println("Contém funcionando? " + (contem(origem, new char[] {'m', 'u', 'n', 'd', 'o', 's'}) == false));
        
        // Testes do método substring()
        System.out.println("Substring funcionando? " + (Arrays.equals(substring(origem, 0, 3), new char[] {'o', 'l', 'a'})));
        System.out.println("Substring funcionando? " + (Arrays.equals(substring(origem, 0, 5), new char[] {'o', 'l', 'a', ' ', 'm'})));
        System.out.println("Substring funcionando? " + (substring(origem, 0, 100) == null));
        System.out.println("Substring funcionando? " + (substring(origem, -100, 2) == null));
        System.out.println("Substring funcionando? " + (substring(origem, 5, 2) == null));
        
        // Testes do método indexOf()
        System.out.println("IndexOf funcionando? " + (indexOf(origem, new char[] {'o', 'l', 'a'}) == 0));
        System.out.println("IndexOf funcionando? " + (indexOf(origem, new char[] {'m', 'u', 'n'}) == 4));
        System.out.println("IndexOf funcionando? " + (indexOf(origem, new char[] {'m', 'a', 'n'}) == -1));
    }

}